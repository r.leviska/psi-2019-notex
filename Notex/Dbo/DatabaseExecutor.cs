﻿using Dbo.Models;
using System;
using System.Data.SqlClient;

namespace Dbo
{
    public class DatabaseExecutor
    {
        private string connetionString = @"Server=(localdb)\madder;Database=notex;Trusted_Connection=True;";

        public void CreateAccount(Account acc)
        {
            var bdoConnection = new SqlConnection(connetionString);
            bdoConnection.Open();
            using (SqlCommand command = new SqlCommand(
                $"Insert Into [Account] values ('{acc.Name}' , '{acc.Password}' , '{acc.Email}' , '{"041212433"}' , '{acc.Age}')",
                bdoConnection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                }
            }
            bdoConnection.Close();
        }

        public Account ReturnAccount(string id)
        {
            var bdoConnection = new SqlConnection(connetionString);
            bdoConnection.Open();
            using (SqlCommand command = new SqlCommand(
                $"Select * from [Account] where Guid={id}",
                bdoConnection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        return new Account
                        {
                            Name = reader["Name"].ToString(),
                            Age = DateTime.Parse( reader["Age"].ToString()),
                            Password = reader["Password"].ToString(),
                            Email = reader["Email"].ToString(),
                            Guid = reader["Guid"].ToString()
                        };
                    }
                }
            }
            bdoConnection.Close();
            return null;
        }

        public void DeleteAccount(string id)
        {
            var bdoConnection = new SqlConnection(connetionString);
            bdoConnection.Open();
            using (SqlCommand command = new SqlCommand(
                $"Delete from [Account] where Guid={id}",
                bdoConnection))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var rer = reader;

                }
            }
            bdoConnection.Close();
        }
    }
}