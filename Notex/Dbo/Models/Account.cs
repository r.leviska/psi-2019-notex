﻿using System;

namespace Dbo.Models
{
    public class Account
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public DateTime Age { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public string Guid { get; set; }
    }
}