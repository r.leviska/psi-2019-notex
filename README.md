# PSI-2019-Notex

USE CASES:

1. Create An Account
2. Facebook & Instagram login integration
3. Upload Pictures
4. Chat With Other Users
5. Create Profile Description
6. Like/Dislike/Super-Like Other Users' Photos
7. Undo Like/Dislike/Super-Like
8. Search Liked Users
9. Search Liked Users By Categories
10. Option To Show Only Online Users
11. Option To Show Statistics About Your Own Profile (Like/Dislike Ratio)
12. Option To Show Your Exact Location
13. Option To Report A Bug
14. Option To Report A User
15. Option To Leave Feedback About The App
16. Option To Choose Radius In Which To Show Users (In Km)
17. Option To Select Age Range In Which To Show Users
18. Option To return to last watched person
19. Option To sleep your account
20. Delete An Account